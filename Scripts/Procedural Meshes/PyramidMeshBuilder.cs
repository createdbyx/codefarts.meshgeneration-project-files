namespace Codefarts.MeshGeneration.ProceduralMeshes
{
    using Codefarts.MeshGeneration;

    using UnityEngine;

    [RequireComponent(typeof(MeshFilter), typeof(MeshCollider), typeof(MeshRenderer)), ExecuteInEditMode]
    public class PyramidMeshBuilder : MonoBehaviour
    {
        internal static Mesh sharedInstance;

        public static Mesh SharedInstance
        {
            get
            {
                if (sharedInstance == null)
                {
                    sharedInstance = Generate.Pyramid();
                    sharedInstance.RecalculateBounds();
                    sharedInstance.Optimize();
                }

                return sharedInstance;
            }
        }

        private void Awake()
        {
            var filter = this.GetComponent<MeshFilter>();

            filter.sharedMesh = null;
            filter.sharedMesh = PyramidMeshBuilder.SharedInstance;

            var collider = this.GetComponent<MeshCollider>();
            if (collider != null)
            {
                collider.sharedMesh = null;
                collider.sharedMesh = filter.sharedMesh;
                collider.convex = true;
            }
        }
    }
}