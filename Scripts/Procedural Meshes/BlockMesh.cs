// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.MeshGeneration.ProceduralMeshes
{
    using Codefarts.MeshGeneration;

    using UnityEngine;

    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer)), ExecuteInEditMode]
    public class BlockMesh : MonoBehaviour
    {
        internal static Mesh sharedInstance;

        public static Mesh SharedInstance
        {
            get
            {
                if (sharedInstance == null)
                {
                    sharedInstance = Generate.MultipleMaterialCube(1);
                    sharedInstance.RecalculateBounds();
                    sharedInstance.Optimize();
                }

                return sharedInstance;
            }
        }

        private void Awake()
        {
            var filter = this.GetComponent<MeshFilter>();

            filter.sharedMesh = null;
            filter.sharedMesh = BlockMesh.SharedInstance;

            var collider = this.GetComponent<BoxCollider>();
            if (collider != null)
            {
                collider.size = new Vector3(0, 0, 0.5f);
            }
        }
    }
}