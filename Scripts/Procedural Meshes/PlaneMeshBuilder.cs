namespace Codefarts.MeshGeneration.ProceduralMeshes
{
    using System;

    using Codefarts.MeshGeneration;

    using UnityEngine;

    /// <summary>
    /// Provides a behaviour for generating a procedural plane mesh.
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer)), ExecuteInEditMode]
    public class PlaneMeshBuilder : MonoBehaviour
    {
        /// <summary>
        /// The width value used when the plane is generated.
        /// </summary>
        [SerializeField]
        private float width = 1;

        /// <summary>
        /// The height value used when the plane is generated.
        /// </summary>
        [SerializeField]
        private float height = 1;

        /// <summary>
        /// The number of columns the generated plane will have.
        /// </summary>
        [SerializeField]
        private int columns = 1;

        /// <summary>
        /// The number of rows the generated plane will have.
        /// </summary>
        [SerializeField]
        private int rows = 1;

        /// <summary>
        /// Holds a flag indicating weather opr not hte plane mesh needs to be rebuilt.
        /// </summary>
        private bool needsRebuild;

        /// <summary>
        /// The offset u value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float offsetU;

        /// <summary>
        /// The offset v value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float offsetV;

        /// <summary>
        /// The scale u value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float scaleU = 1;

        /// <summary>
        /// The scale v value that will be applied to each generated triangle texture coordinate.
        /// </summary>
        [SerializeField]
        private float scaleV = 1;

        /// <summary>
        /// Gets or sets the width of the generated plane.
        /// </summary>
        public virtual float Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.needsRebuild = this.width != value || this.needsRebuild;
                this.width = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the generated plane.
        /// </summary>
        public virtual float Height
        {
            get
            {
                return this.height;
            }

            set
            {
                this.needsRebuild = this.height != value || this.needsRebuild;
                this.height = value;
            }
        }

        /// <summary>
        /// Gets or sets the offset u that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float OffsetU
        {
            get
            {
                return this.offsetU;
            }

            set
            {
                this.needsRebuild = this.offsetU != value || this.needsRebuild;
                this.offsetU = value;
            }
        }

        /// <summary>
        /// Gets or sets the offset v that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float OffsetV
        {
            get
            {
                return this.offsetV;
            }

            set
            {
                this.needsRebuild = this.offsetV != value || this.needsRebuild;
                this.offsetV = value;
            }
        }

        /// <summary>
        /// Gets or sets the scale u that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float ScaleU
        {
            get
            {
                return this.scaleU;
            }

            set
            {
                this.needsRebuild = this.scaleU != value || this.needsRebuild;
                this.scaleU = value;
            }
        }

        /// <summary>
        /// Gets or sets the scale v that will be applied to each triangle across the plane.
        /// </summary>
        public virtual float ScaleV
        {
            get
            {
                return this.scaleV;
            }

            set
            {
                this.needsRebuild = this.scaleV != value || this.needsRebuild;
                this.scaleV = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of columns that make up the generated plane.
        /// </summary>
        public virtual int Columns
        {
            get
            {
                return this.columns;
            }

            set
            {
                // cap value to prevent 65000 vertex limit and prevent exceptions
                value = Mathf.Clamp(value, 1, 254 - this.rows);
                value = Math.Max(1, value);
                this.needsRebuild = this.columns != value || this.needsRebuild;
                this.columns = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of rows that make up the generated plane.
        /// </summary>
        public virtual int Rows
        {
            get
            {
                return this.rows;
            }

            set
            {
                // cap value to prevent 65000 vertex limit and prevent exceptions
                value = Mathf.Clamp(value, 1, 254 - this.columns);
                value = Math.Max(1, value);
                this.needsRebuild = this.rows != value || this.needsRebuild;
                this.rows = value;
            }
        }

        //internal static Mesh sharedInstance;

        //public static Mesh SharedInstance
        //{
        //    get
        //    {
        //        if (sharedInstance == null)
        //        {
        //            sharedInstance = Generate.Plane(this.Width,this.hideFlags,this.Columns,this.Rows, 0, 0, 1, 1);
        //            sharedInstance.RecalculateBounds();
        //            sharedInstance.Optimize();
        //        }

        //        return sharedInstance;
        //    }
        //}
        
        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public virtual void Awake()
        {
            this.BuildMesh();   
        }

        /// <summary>
        /// Used to rebuild the plane mesh.
        /// </summary>
        private void BuildMesh()
        {
            var filter = this.GetComponent<MeshFilter>();

            filter.sharedMesh = null;
            var mesh = Generate.Plane(this.width, this.height, this.columns, this.rows, this.offsetU, this.offsetV, this.scaleU, this.scaleV);
            mesh.RecalculateBounds();
            mesh.Optimize();
            filter.sharedMesh = mesh;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.needsRebuild)
            {
                this.BuildMesh();
                this.needsRebuild = false;
            }
        }
    }
}