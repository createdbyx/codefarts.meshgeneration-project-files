﻿namespace Codefarts.MeshGeneration.Editor
{
    using Codefarts.MeshGeneration.ProceduralMeshes;
    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides a editor for the <see cref="PlaneMeshBuilder"/> behaviour.
    /// </summary>
    [CustomEditor(typeof(PlaneMeshBuilder))]
    public class PlaneMeshBuilderEditor : Editor
    {
        /// <summary>
        /// Called by unity to draw the inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var builder = this.target as PlaneMeshBuilder;

            if (builder == null)
            {
                return;
            }

            builder.Columns = EditorGUILayout.IntField("Columns", builder.Columns);
            builder.Rows = EditorGUILayout.IntField("Rows", builder.Rows);

            var vector2 = EditorGUILayout.Vector2Field("UV Offset", new Vector2(builder.OffsetU, builder.OffsetV));
            builder.OffsetU = vector2.x;
            builder.OffsetV = vector2.y;

            vector2 = EditorGUILayout.Vector2Field("UV Scale", new Vector2(builder.ScaleU, builder.ScaleV));
            builder.ScaleU = vector2.x;
            builder.ScaleV = vector2.y;

            EditorUtility.SetDirty(builder);
        }
    }
}
