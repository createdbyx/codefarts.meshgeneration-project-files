namespace Codefarts.MeshGeneration
{
    using System;

#if PERFORMANCE
    using Codefarts.PerformanceTesting;
#endif

    using UnityEngine;

    /// <summary>
    /// Provides methods that are used to generate various basic mesh shapes.
    /// </summary>
    public partial class Generate
    {
        public static Mesh CornerRamp(Vector3 size)
        {
            var mesh = new Mesh();
            var halfX = size.x == 0 ? 0 : size.x / 2f;
            var halfY = size.y == 0 ? 0 : size.y / 2f;
            var halfZ = size.z == 0 ? 0 : size.z / 2f;

            var vertices = new Vector3[]
                {
                    new Vector3(-halfX, halfY, -halfZ),
                    new Vector3(halfX, -halfY, -halfZ),
                    new Vector3(-halfX, -halfY, -halfZ),
                    new Vector3(halfX, -halfY, -halfZ),
                    new Vector3(-halfX, -halfY, halfZ),
                    new Vector3(-halfX, -halfY, -halfZ),
                    new Vector3(halfX, -halfY, halfZ),
                    new Vector3(-halfX, -halfY, -halfZ),
                    new Vector3(-halfX, -halfY, halfZ),
                    new Vector3(-halfX, halfY, -halfZ),
                    new Vector3(-halfX, halfY, -halfZ),
                    new Vector3(halfX, -halfY, halfZ),
                    new Vector3(halfX, -halfY, -halfZ),
                    new Vector3(-halfX, halfY, -halfZ),
                    new Vector3(-halfX, -halfY, halfZ),
                    new Vector3(halfX, -halfY, halfZ),
                };

            var normals = new Vector3[]
                {
                    new Vector3(0f, 0f, -1f),
                    new Vector3(0f, 0f, -1f),
                    new Vector3(0f, 0f, -1f),
                    new Vector3(0f, -1f, 0f),
                    new Vector3(0f, -1f, 0f),
                    new Vector3(0f, -1f, 0f),
                    new Vector3(0f, -1f, 0f),
                    new Vector3(-1f, 0f, 0f),
                    new Vector3(-1f, 0f, 0f),
                    new Vector3(-1f, 0f, 0f),
                    new Vector3(1f, 1f, 0f),
                    new Vector3(1f, 1f, 0f),
                    new Vector3(1f, 1f, 0f),
                    new Vector3(0f, 1f, 1f),
                    new Vector3(0f, 1f, 1f),
                    new Vector3(0f, 1f, 1f),
                };

            var uv = new Vector2[]
                {
                    new Vector2(0.0004994869f, 0.9955829f),
                    new Vector2(0.9955829f, 0.0004994869f),
                    new Vector2(0.0004994869f, 0.0004994869f),
                    new Vector2(0.0004994869f, 0.0004994869f),
                    new Vector2(0.9955829f, 0.9955829f),
                    new Vector2(0.9955829f, 0.0004994869f),
                    new Vector2(0.0004994869f, 0.9955829f),
                    new Vector2(0.0004994869f, 0.0004994869f),
                    new Vector2(0.0004994869f, 0.9955829f),
                    new Vector2(0.9955829f, 0.0004994869f),
                    new Vector2(0.9955829f, 0.0004994869f),
                    new Vector2(0.0004994869f, 0.9955829f),
                    new Vector2(0.0004994869f, 0.0004994869f),
                    new Vector2(0.9955829f, 0.0004994869f),
                    new Vector2(0.9955829f, 0.9955829f),
                    new Vector2(0.0004994869f, 0.9955829f),
                };

            var triangles = new int[]
                {
                    0, 1, 2,
                    3, 4, 5,
                    4, 3, 6,
                    7, 8, 9,
                    10, 11, 12,
                    13, 14, 15,
                };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "CornerRampFromCode";
            return mesh;
        }

        public static Mesh Plane(float width, float height, int columns, int rows)
        {
            return Plane(width, height, columns, rows, 0, 0, 1, 1);
        }

        public static Mesh Plane(float width, float height, int columns, int rows, float offsetU, float offsetV, float scaleU, float scaleV)
        {
            if (columns < 1)
            {
                throw new ArgumentOutOfRangeException("columns");
            }

            if (rows < 1)
            {
                throw new ArgumentOutOfRangeException("rows");
            }

            var vertexCount = columns * rows * 4;
            if (vertexCount > 65000)
            {
                throw new ArgumentOutOfRangeException("Too many vertexes. Reduce Column/Row count.");
            }

            var mesh = new Mesh();

            var halfX = width / 2;
            var halfY = height / 2;

            var cellWidth = width / columns;
            var cellHeight = width / rows;

            var vertices = new Vector3[vertexCount];
            var normals = new Vector3[vertexCount];
            var uv = new Vector2[vertexCount];
            var triangles = new int[columns * rows * 6];
            var triangleIndex = 0;

            for (var y = 0; y < rows; y++)
            {
                for (var x = 0; x < columns; x++)
                {
                    // calculate index ((row * number of columns) + column) * number of vertexes per cell
                    var index = ((y * columns) + x) * 4;

                    // calculate cell bounds
                    var cellLeft = x * cellWidth;
                    var cellTop = y * cellHeight;
                    var cellRight = cellLeft + cellWidth;
                    var cellBottom = cellTop + cellHeight;

                    // calculate vertexes for cell
                    vertices[index] = new Vector3(cellLeft - halfX, 0, cellTop - halfY);            // top left
                    vertices[index + 1] = new Vector3(cellRight - halfX, 0, cellTop - halfY);       // top right
                    vertices[index + 2] = new Vector3(cellRight - halfX, 0, cellBottom - halfY);    // bottom right
                    vertices[index + 3] = new Vector3(cellLeft - halfX, 0, cellBottom - halfY);     // bottom left

                    // calculate normals for cell
                    normals[index] = vertices[index] + Vector3.up;
                    normals[index + 1] = vertices[index + 1] + Vector3.up;
                    normals[index + 2] = vertices[index + 2] + Vector3.up;
                    normals[index + 3] = vertices[index + 3] + Vector3.up;

                    // calculate uv's for cell
                    uv[index] = new Vector2(((cellLeft / width) * scaleU) - offsetU, ((cellTop / height) * scaleV) - offsetV);
                    uv[index + 1] = new Vector2(((cellRight / width) * scaleU) - offsetU, ((cellTop / height) * scaleV) - offsetV);
                    uv[index + 2] = new Vector2(((cellRight / width) * scaleU) - offsetU, ((cellBottom / height) * scaleV) - offsetV);
                    uv[index + 3] = new Vector2(((cellLeft / width) * scaleU) - offsetU, ((cellBottom / height) * scaleV) - offsetV);

                    // construct 2 counter clockwise triangles for cell
                    triangles[triangleIndex++] = index;
                    triangles[triangleIndex++] = index + 3;
                    triangles[triangleIndex++] = index + 1;
                    triangles[triangleIndex++] = index + 1;
                    triangles[triangleIndex++] = index + 3;
                    triangles[triangleIndex++] = index + 2;
                }
            }

            //var vertices = new Vector3[]
            //    {   
            //        new Vector3(-halfX, 0, -halfY),
            //        new Vector3(-halfX, 0, halfY),
            //        new Vector3(halfX, 0, halfY),
            //        new Vector3(halfX, 0, -halfY),
            //    };

            //var normals = new Vector3[]
            //    {
            //        new Vector3(-halfX, 1, -halfY),                                                                               
            //        new Vector3(-halfX, 1, halfY),
            //        new Vector3(halfX, 1, halfY),
            //        new Vector3(halfX, 1, -halfY),
            //    };

            //var uv = new Vector2[]
            //    {
            //        new Vector2((0 * scaleU) - offsetU, (0 * scaleV) - offsetV),
            //        new Vector2((0 * scaleU) - offsetU, (1 * scaleV) - offsetV),
            //        new Vector2((1 * scaleU) - offsetU, (1 * scaleV) - offsetV),
            //        new Vector2((1 * scaleU) - offsetU, (0 * scaleV) - offsetV),
            //    };

            //var triangles = new int[]
            //    {
            //        0, 1, 2,
            //        0, 2, 3,
            //    };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "PlaneFromCode";
            return mesh;
        }

        public static Mesh Strip(float width, float height, int columns)
        {
            if (columns < 1)
            {
                throw new ArgumentOutOfRangeException("columns");
            }

            var mesh = new Mesh();

            var vertices = new Vector3[(columns + 1) * 2];
            var normals = new Vector3[(columns + 1) * 2];
            var uv = new Vector2[(columns + 1) * 2];
            for (var i = 0; i < columns; i += 2)
            {
                vertices[i] = new Vector3((i / columns) * width, 0, 0);
                vertices[i + 1] = new Vector3((i / columns) * width, 0, height);

                normals[i] = new Vector3((i / columns) * width, 1, 0);
                normals[i + 1] = new Vector3((i / columns) * width, 1, height);

                uv[i] = new Vector2(i / columns, 0);
                uv[i + 1] = new Vector2(i / columns, 1);
            }

            var triangles = new int[(columns * 2) * 3];
            for (var i = 0; i < columns; i += 2)
            {
                triangles[i] = i;
                triangles[i + 1] = i + 1;
                triangles[i + 2] = i + 2;

                triangles[i + 1] = i + 1;
                triangles[i + 2] = i + 2;
                triangles[i + 3] = i + 3;
            }

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "StripFromCode";
            return mesh;
        }

        public static Mesh OutsideCornerCube()
        {
            var mesh = new Mesh();


            var vertices = new Vector3[]
            {
                new Vector3(0.308f, -0.088f, -0.5f),
                new Vector3(0.087f, -0.309f, -0.5f),
                new Vector3(-0.191f, -0.451f, -0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0.45f, 0.19f, -0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, 0.5f),
                new Vector3(-0.5f, 0.5f, 0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, 0.5f),
                new Vector3(0.308f, -0.088f, 0.5f),
                new Vector3(-0.5f, -0.5f, 0.5f),
                new Vector3(-0.191f, -0.451f, 0.5f),
                new Vector3(0.45f, 0.19f, 0.5f),
                new Vector3(-0.5f, 0.5f, 0.5f),
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(0.087f, -0.309f, 0.5f),
                new Vector3(0.45f, 0.19f, 0.5f),
                new Vector3(0.45f, 0.19f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(0.308f, -0.088f, 0.5f),
                new Vector3(0.308f, -0.088f, -0.5f),
                new Vector3(0.087f, -0.309f, 0.5f),
                new Vector3(0.087f, -0.309f, -0.5f),
                new Vector3(-0.191f, -0.451f, 0.5f),
                new Vector3(-0.191f, -0.451f, -0.5f),
                new Vector3(-0.5f, -0.5f, 0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
            };

            var normals = new Vector3[]
            {
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-0.016f, -0.016f, -1f),
                new Vector3(-1f, -0.016f, 0f),
                new Vector3(-1f, -0.016f, 0f),
                new Vector3(-1f, -0.016f, 0f),
                new Vector3(-1f, -0.016f, 0f),
                new Vector3(-0.016f, 1f, 0f),
                new Vector3(-0.016f, 1f, 0f),
                new Vector3(-0.016f, 1f, 0f),
                new Vector3(-0.016f, 1f, 0f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(-0.016f, -0.016f, 1f),
                new Vector3(0.937f, -0.333f, -0.109f),
                new Vector3(0.937f, -0.333f, -0.109f),
                new Vector3(0.968f, -0.175f, -0.179f),
                new Vector3(0.968f, -0.175f, -0.179f),
                new Vector3(0.778f, -0.619f, -0.109f),
                new Vector3(0.778f, -0.619f, -0.109f),
                new Vector3(0.587f, -0.809f, 0f),
                new Vector3(0.587f, -0.809f, 0f),
                new Vector3(0.297f, -0.955f, 0f),
                new Vector3(0.297f, -0.955f, 0f),
                new Vector3(0.141f, -0.99f, 0f),
                new Vector3(0.141f, -0.99f, 0f),
            };

            var uv = new Vector2[]
            {
                new Vector2(0.8078431f, 0.4117647f),
                new Vector2(0.5843137f, 0.1882352f),
                new Vector2(0.3058823f, 0.04705876f),
                new Vector2(-5.960464E-08f, -5.960464E-08f),
                new Vector2(0.9490196f, 0.690196f),
                new Vector2(-5.960464E-08f, 0.9960784f),
                new Vector2(0.9960784f, 0.9960784f),
                new Vector2(-5.960464E-08f, -5.960464E-08f),
                new Vector2(-5.960464E-08f, 0.9960784f),
                new Vector2(0.9960784f, 0.9960784f),
                new Vector2(0.9960784f, -5.960464E-08f),
                new Vector2(0.9960784f, 0.9960784f),
                new Vector2(0.9960784f, -5.960464E-08f),
                new Vector2(-5.960464E-08f, -5.960464E-08f),
                new Vector2(-5.960464E-08f, 0.9960784f),
                new Vector2(0.1882352f, 0.4117647f),
                new Vector2(0.9960784f, -5.960464E-08f),
                new Vector2(0.690196f, 0.04705876f),
                new Vector2(0.04705876f, 0.690196f),
                new Vector2(0.9960784f, 0.9960784f),
                new Vector2(-5.960464E-08f, 0.9960784f),
                new Vector2(0.4117647f, 0.1882352f),
                new Vector2(0.1764705f, 0.9960784f),
                new Vector2(0.1764705f, -5.960464E-08f),
                new Vector2(-5.960464E-08f, -5.960464E-08f),
                new Vector2(-5.960464E-08f, 0.9960784f),
                new Vector2(0.3882352f, 0.9960784f),
                new Vector2(0.3882352f, -5.960464E-08f),
                new Vector2(0.6078431f, 0.9960784f),
                new Vector2(0.6078431f, -5.960464E-08f),
                new Vector2(0.8196078f, 0.9960784f),
                new Vector2(0.8196078f, -5.960464E-08f),
                new Vector2(0.9960784f, 0.9999999f),
                new Vector2(0.9999999f, -5.960464E-08f),
            };

            var triangles = new int[]
            {
                0, 1, 2,
                2, 3, 0,
                3, 4, 0,
                3, 5, 4,
                5, 6, 4,
                7, 8, 9,
                9, 10, 7,
                11, 12, 13,
                13, 14, 11,
                15, 16, 17,
                15, 18, 16,
                18, 19, 16,
                18, 20, 19,
                17, 21, 15,
                22, 23, 24,
                24, 25, 22,
                23, 22, 26,
                26, 27, 23,
                27, 26, 28,
                28, 29, 27,
                29, 28, 30,
                30, 31, 29,
                31, 30, 32,
                32, 33, 31,
            };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "OutsideCornerFromCode";

            return mesh;
        }

        public static Mesh Pyramid()
        {
            var mesh = new Mesh();

            var vertices = new Vector3[]
            {
                new Vector3(0f, 0f, 0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0f, 0f, 0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0f, 0f, 0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(0f, 0f, 0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
            };

            var normals = new Vector3[]
            {
                new Vector3(-0.014f, -0.894f, 0.447f),
                new Vector3(-0.014f, -0.894f, 0.447f),
                new Vector3(-0.014f, -0.894f, 0.447f),
                new Vector3(-0.894f, -0.014f, 0.447f),
                new Vector3(-0.894f, -0.014f, 0.447f),
                new Vector3(-0.894f, -0.014f, 0.447f),
                new Vector3(-0.014f, 0.894f, 0.447f),
                new Vector3(-0.014f, 0.894f, 0.447f),
                new Vector3(-0.014f, 0.894f, 0.447f),
                new Vector3(0.894f, -0.014f, 0.447f),
                new Vector3(0.894f, -0.014f, 0.447f),
                new Vector3(0.894f, -0.014f, 0.447f),
                new Vector3(-0.014f, -0.014f, -1f),
                new Vector3(-0.014f, -0.014f, -1f),
                new Vector3(-0.014f, -0.014f, -1f),
                new Vector3(-0.014f, -0.014f, -1f),
            };

            var uv = new Vector2[]
            {
                new Vector2(0.4980412f, 0.9955828f),
                new Vector2(0.9955828f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.0004995465f),
                new Vector2(0.4980412f, 0.9955828f),
                new Vector2(0.9955828f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.0004995465f),
                new Vector2(0.4980412f, 0.9955828f),
                new Vector2(0.9955828f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.0004995465f),
                new Vector2(0.4980412f, 0.9955828f),
                new Vector2(0.9955828f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.0004995465f),
                new Vector2(0.0004995465f, 0.9955828f),
                new Vector2(0.4980412f, 0.4980412f),
                new Vector2(0.9955828f, 0.9955828f),
            };

            var triangles = new int[]
            {
                0, 1, 2,
                3, 4, 5,
                6, 7, 8,
                9, 10, 11,
                12, 13, 14,
                13, 15, 14,
            };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "PyramidFromCode";

            return mesh;
        }

        /// <summary>
        /// Creates a three material cube mesh.
        /// </summary>
        /// <param name="height">The height of the cube.</param>
        /// <returns>Returns a <see cref="Mesh"/> reference for the generated cube.</returns>
        public static Mesh MultipleMaterialCube(float height)
        {
            return MultipleMaterialCube(1, height, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 3);
        }

        /// <summary>
        /// Creates a three material cube mesh.
        /// </summary>
        /// <param name="width">The width of the cube.</param>
        /// <param name="height">The height of the cube.</param>
        /// <param name="depth">The depth of the cube.</param>
        /// <returns>Returns a <see cref="Mesh"/> reference for the generated cube.</returns>
        public static Mesh MultipleMaterialCube(float width, float height, float depth)
        {
            return MultipleMaterialCube(width, height, depth, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 3);
        }

        /// <summary>
        /// Creates a multiple material cube mesh.
        /// </summary>
        /// <param name="width">The width of the cube.</param>
        /// <param name="height">The height of the cube.</param>
        /// <param name="depth">The depth of the cube.</param>
        /// <param name="wallOffsetU">The wall u offset.</param>
        /// <param name="wallOffsetV">The wall v offset.</param>
        /// <param name="wallScaleU">The wall u scale.</param>
        /// <param name="wallScaleV">The wall v scale.</param>
        /// <param name="topOffsetU">The top u offset.</param>
        /// <param name="topOffsetV">The top v offset.</param>
        /// <param name="topScaleU">The top u scale.</param>
        /// <param name="topScaleV">The top v scale.</param>
        /// <param name="bottomOffsetU">The u bottom offset.</param>
        /// <param name="bottomOffsetV">The v bottom offset.</param>
        /// <param name="bottomScaleU">The u bottom scale.</param>
        /// <param name="bottomScaleV">The v bottom scale.</param>
        /// <param name="materialCount">Defines how many materials the generated <see cref="Mesh"/> will make use of. See remarks for details.</param>
        /// <returns>Returns a <see cref="Mesh"/> reference for the generated cube.</returns>
        /// <remarks><p>Values for <see cref="materialCount"/> ar as fallows.</p>
        /// <p>
        /// 1: Cube will be build with one material.<br />
        /// 2: Cube will be build with two materials, one for the sides and one for the top and bottom.<br />
        /// 3: Cube will be build with three materials, one for the sides, one for the top & one for the bottom.<br/>
        /// If any other value is specified a three materials cube will be generated.</p></remarks>
        public static Mesh MultipleMaterialCube(
            float width,
            float height,
            float depth,
            float wallOffsetU,
            float wallOffsetV,
            float wallScaleU,
            float wallScaleV,
            float topOffsetU,
            float topOffsetV,
            float topScaleU,
            float topScaleV,
            float bottomOffsetU,
            float bottomOffsetV,
            float bottomScaleU,
            float bottomScaleV,
            int materialCount)
        {
#if PERFORMANCE
            var perf = PerformanceTesting<string>.Instance;
            perf.Start(PerformanceConstants.CreateThreeMaterialBlock);
#endif

            try
            {
                var mesh = new Mesh();
                var halfWidth = width / 2f;
                var halfHeight = height / 2f;
                var halfDepth = depth / 2f;
                var vertices = new Vector3[]
                    {
                        new Vector3(-halfWidth, -halfHeight, halfDepth),    // 0 bottom 
                        new Vector3(halfWidth,  -halfHeight, halfDepth),    // 1 bottom
                        new Vector3(halfWidth,  -halfHeight, -halfDepth),   // 2 bottom
                        new Vector3(-halfWidth, -halfHeight, -halfDepth),   // 3 bottom

                        new Vector3(-halfWidth, halfHeight, halfDepth),     // 4 top
                        new Vector3(halfWidth,  halfHeight, halfDepth),     // 5 top
                        new Vector3(halfWidth,  halfHeight, -halfDepth),    // 6 top
                        new Vector3(-halfWidth, halfHeight, -halfDepth),    // 7 top

                        new Vector3(halfWidth,  -halfHeight, -halfDepth),   // 8 back
                        new Vector3(-halfWidth, halfHeight, -halfDepth),    // 9 back
                        new Vector3(-halfWidth, -halfHeight, -halfDepth),   // 10 back
                        new Vector3(halfWidth,  halfHeight, -halfDepth),    // 11 back
                                                        
                        new Vector3(-halfWidth, -halfHeight, -halfDepth),   // 12 left
                        new Vector3(-halfWidth, halfHeight, halfDepth),     // 13 left
                        new Vector3(-halfWidth, -halfHeight, halfDepth),    // 14 left
                        new Vector3(-halfWidth, halfHeight, -halfDepth),    // 15 left
                                                       
                        new Vector3(-halfWidth, -halfHeight,  halfDepth),   // 16 front
                        new Vector3(halfWidth,  halfHeight, halfDepth),     // 17 front 
                        new Vector3(halfWidth,  -halfHeight, halfDepth),    // 18 front 
                        new Vector3(-halfWidth, halfHeight,  halfDepth),    // 19 front
                                                              
                        new Vector3(halfWidth,  -halfHeight, halfDepth),    // 20 right
                        new Vector3(halfWidth,  halfHeight, -halfDepth),    // 21 right
                        new Vector3(halfWidth,  -halfHeight, -halfDepth),   // 22 right
                        new Vector3(halfWidth,  halfHeight, halfDepth),     // 23 right
                    };

                var normals = new Vector3[]
                    {
                        new Vector3(0f, -1f, 0f),     // bottom  0
                        new Vector3(0f, -1f, 0f),     // bottom  1
                        new Vector3(0f, -1f, 0f),     // bottom  2
                        new Vector3(0f, -1f, 0f),     // bottom  3
                        new Vector3(0f, 1f, 0f),      // top     4
                        new Vector3(0f, 1f, 0f),      // top     5
                        new Vector3(0f, 1f, 0f),      // top     6
                        new Vector3(0f, 1f, 0f),      // top     7
                        new Vector3(0f,  0f, -1f),    // back    8
                        new Vector3(0f,  0f, -1f),    // back    9
                        new Vector3(0f,  0f, -1f),    // back    10
                        new Vector3(0f,  0f, -1f),    // back    11
                        new Vector3(-1f, 0f, 0f),     // left    12
                        new Vector3(-1f, 0f, 0f),     // left    13
                        new Vector3(-1f, 0f, 0f),     // left    14
                        new Vector3(-1f, 0f, 0f),     // left    15
                        new Vector3(0f,  0f, 1f),     // front   16
                        new Vector3(0f,  0f, 1f),     // front   17
                        new Vector3(0f,  0f, 1f),     // front   18
                        new Vector3(0f,  0f, 1f),     // front   19
                        new Vector3(1f,  0f, 0f),     // right   20
                        new Vector3(1f,  0f, 0f),     // right   21
                        new Vector3(1f,  0f, 0f),     // right   22
                        new Vector3(1f,  0f, 0f),     // right   23
                    };

                var uv = new Vector2[]                      
                    {                                       
                        new Vector2((0f * bottomScaleU) - bottomOffsetU, (0f * bottomScaleV) - bottomOffsetV), // 0 bottom
                        new Vector2((1f * bottomScaleU) - bottomOffsetU, (0f * bottomScaleV) - bottomOffsetV), // 1 bottom
                        new Vector2((1f * bottomScaleU) - bottomOffsetU, (1f * bottomScaleV) - bottomOffsetV), // 2 bottom
                        new Vector2((0f * bottomScaleU) - bottomOffsetU, (1f * bottomScaleV) - bottomOffsetV), // 3 bottom
                        
                        new Vector2((1f * topScaleU) - topOffsetU, (0f * topScaleV) - topOffsetV),             // 4 top
                        new Vector2((0f * topScaleU) - topOffsetU, (0f * topScaleV) - topOffsetV),             // 5 top
                        new Vector2((0f * topScaleU) - topOffsetU, (1f * topScaleV) - topOffsetV),             // 6 top
                        new Vector2((1f * topScaleU) - topOffsetU, (1f * topScaleV) - topOffsetV),             // 7 top
                                                                                                               
                        new Vector2((1f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 8 back
                        new Vector2((0f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 9 back
                        new Vector2((0f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 10 back
                        new Vector2((1f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 11 back
                                                                                                               
                        new Vector2((1f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 12 left
                        new Vector2((0f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 13 left
                        new Vector2((0f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 14 left
                        new Vector2((1f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 15 left
                                                                                                               
                        new Vector2((1f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 16 front
                        new Vector2((0f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 17 front
                        new Vector2((0f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 18 front
                        new Vector2((1f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 19 front
                                                                                                               
                        new Vector2((1f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 20 right
                        new Vector2((0f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 21 right
                        new Vector2((0f * wallScaleU) - wallOffsetU, (0f * wallScaleV) - wallOffsetV),         // 22 right
                        new Vector2((1f * wallScaleU) - wallOffsetU, (1f * wallScaleV) - wallOffsetV),         // 23 right   
                    };

                // assign vertexes, normals & uv's
                mesh.vertices = vertices;
                mesh.normals = normals;
                mesh.uv = uv;

                // based on material count assign triangles
                switch (materialCount)
                {
                    case 1:
                        var triangles = new int[]
                         {
                             4, 5, 6,      // top
                             6, 7, 4,      // top
                          
                             1, 0, 3,      // bottom
                             3, 2, 1,      // bottom
                           
                             10, 9, 8,     // back
                             11, 8, 9,     // back
                           
                             14, 13, 12,   // left
                             15, 12, 13,   // left

                             18, 17, 16,   // front
                             19, 16, 17,   // front

                             22, 21, 20,   // right
                             23, 20, 21,   // right
                         };

                        mesh.triangles = triangles;
                        break;

                    case 2:
                        var topBottom = new int[]
                         {
                             4, 5, 6,      // top
                             6, 7, 4,      // top
                           
                             1, 0, 3,      // bottom
                             3, 2, 1,      // bottom
                        };

                        var walls2 = new int[]
                         {
                             10, 9, 8,     // back
                             11, 8, 9,     // back
                           
                             14, 13, 12,   // left
                             15, 12, 13,   // left

                             18, 17, 16,   // front
                             19, 16, 17,   // front

                             22, 21, 20,   // right
                             23, 20, 21,   // right
                         };

                        mesh.subMeshCount = 2;
                        mesh.SetTriangles(topBottom, 0);
                        mesh.SetTriangles(walls2, 1);
                        break;

                    default:
                        var floor = new int[]
                         {
                             4, 5, 6,      // top
                             6, 7, 4,      // top
                         };

                        var walls3 = new int[]
                         {
                             10, 9, 8,     // back
                             11, 8, 9,     // back
                           
                             14, 13, 12,   // left
                             15, 12, 13,   // left

                             18, 17, 16,   // front
                             19, 16, 17,   // front

                             22, 21, 20,   // right
                             23, 20, 21,   // right
                         };

                        var ceiling = new int[]
                         {
                             1, 0, 3,      // bottom
                             3, 2, 1,      // bottom
                         };

                        mesh.subMeshCount = 3;
                        mesh.SetTriangles(ceiling, 0);
                        mesh.SetTriangles(floor, 1);
                        mesh.SetTriangles(walls3, 2);
                        break;
                }

                return mesh;
            }
            finally
            {
#if PERFORMANCE
                perf.Stop(PerformanceConstants.CreateThreeMaterialBlock);
#endif
            }
        }
    }
}