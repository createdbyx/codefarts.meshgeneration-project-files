// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Generate Ellipse.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides methods that are used to generate various basic mesh shapes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.MeshGeneration
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides methods that are used to generate various basic mesh shapes.
    /// </summary>
    public partial class Generate
    {
        #region Public Methods and Operators

        /// <summary>
        /// Constructs a ellipse along the x/z axis.
        /// </summary>
        /// <param name="width">
        /// The width of the ellipse.
        /// </param>
        /// <param name="height">
        /// THe height of the ellipse.
        /// </param>
        /// <param name="triangleCount">
        /// The number of triangles that the ellipse will be comprised of.
        /// </param>
        /// <returns>
        /// A reference to a Mesh object.
        /// </returns>
        public static Mesh Ellipse(float width, float height, int triangleCount)
        {
            var mesh = new Mesh();

            var verticesNum = 1 + triangleCount;
            var trianglesNum = triangleCount;

            if (verticesNum > 65000)
            {
                throw new ArgumentOutOfRangeException("triangleCount", "Can't have more then 65001 triangles.");
            }

            var vertices = new Vector3[verticesNum];
            var normals = new Vector3[verticesNum];
            var uvs = new Vector2[verticesNum];
            var triangles = new int[trianglesNum * 3];

            var halfWidth = width / 2;
            var halfHeight = height / 2;
            var radiusRatio = halfWidth / halfHeight;

            // generate the outer points
            for (var i = 0; i < triangleCount; i++)
            {
                var angle = ((float)i / triangleCount) * (Mathf.PI * 2.0f);
                var vertex = new Vector3(Mathf.Sin(angle), 0.0f, Mathf.Cos(angle));

                vertices[i] = new Vector3(vertex.x * halfWidth, 0.0f, vertex.z * halfHeight);
                normals[i] = new Vector3(0, 1, 0);

                uvs[i] = new Vector2(0.5f + vertex.x * 0.5f * radiusRatio, 0.5f + vertex.z * 0.5f * 1 / radiusRatio);
            }

            // generate central point
            vertices[triangleCount] = new Vector3(0, 0, 0);
            normals[triangleCount] = new Vector3(0, 1, 0);
            uvs[triangleCount] = new Vector2(0.5f, 0.5f);

            // generate triangles
            var triVert = 0;
            var triIdx = 0;
            for (var i = 0; i < triangleCount; i++)
            {
                triangles[triIdx + 0] = triVert + 0;
                triangles[triIdx + 1] = triVert + 1;
                triangles[triIdx + 2] = triangleCount;

                if (i == triangleCount - 1)
                {
                    triangles[triIdx + 1] = 0;
                }

                triVert += 1;
                triIdx += 3;
            }

            // assign vertex data
            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uvs;
            mesh.triangles = triangles;

            // recalculate mesh bounds and optimize
            mesh.Optimize();
            mesh.RecalculateBounds();

            return mesh;
        }

        #endregion
    }
}