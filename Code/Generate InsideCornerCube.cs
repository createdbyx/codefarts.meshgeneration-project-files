// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Generate InsideCornerCube.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides methods that are used to generate various basic mesh shapes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.MeshGeneration
{
    using UnityEngine;

    /// <summary>
    /// Provides methods that are used to generate various basic mesh shapes.
    /// </summary>
    public partial class Generate
    {
        #region Public Methods and Operators

        /// <summary>
        /// Generates a cube with one corner carved out of it.
        /// </summary>
        /// <returns>
        /// A reference to a Mesh object.
        /// </returns>
        public static Mesh InsideCornerCube()
        {
            var mesh = new Mesh();

            var vertices = new[]
                               {
                                   new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(0.5f, -0.5f, -0.5f), 
                                   new Vector3(0.5f, -0.5f, 0.5f), new Vector3(0.5f, 0.5f, -0.5f), new Vector3(0.5f, 0.5f, 0.5f), 
                                   new Vector3(0.5f, -0.5f, 0.5f), new Vector3(0.5f, -0.5f, -0.5f), new Vector3(0.087f, -0.309f, -0.5f), 
                                   new Vector3(0.308f, -0.088f, -0.5f), new Vector3(0.5f, -0.5f, -0.5f), new Vector3(0.45f, 0.19f, -0.5f), 
                                   new Vector3(0.5f, 0.5f, -0.5f), new Vector3(-0.191f, -0.451f, -0.5f), new Vector3(-0.5f, -0.5f, -0.5f), 
                                   new Vector3(0.5f, -0.5f, 0.5f), new Vector3(0.308f, -0.088f, 0.5f), new Vector3(0.087f, -0.309f, 0.5f), 
                                   new Vector3(0.45f, 0.19f, 0.5f), new Vector3(0.5f, 0.5f, 0.5f), new Vector3(-0.191f, -0.451f, 0.5f), 
                                   new Vector3(-0.5f, -0.5f, 0.5f), new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.5f, 0.5f, -0.5f), 
                                   new Vector3(0.45f, 0.19f, -0.5f), new Vector3(0.45f, 0.19f, 0.5f), new Vector3(0.308f, -0.088f, -0.5f), 
                                   new Vector3(0.308f, -0.088f, 0.5f), new Vector3(-0.191f, -0.451f, -0.5f), 
                                   new Vector3(-0.191f, -0.451f, 0.5f), new Vector3(0.087f, -0.309f, 0.5f), 
                                   new Vector3(0.087f, -0.309f, -0.5f), new Vector3(0.308f, -0.088f, 0.5f), 
                                   new Vector3(0.308f, -0.088f, -0.5f), new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(-0.5f, -0.5f, 0.5f) 
                               };

            var normals = new[]
                              {
                                  new Vector3(-0.016f, -1f, 0f), new Vector3(-0.016f, -1f, 0f), new Vector3(-0.016f, -1f, 0f), 
                                  new Vector3(-0.016f, -1f, 0f), new Vector3(1f, -0.016f, 0f), new Vector3(1f, -0.016f, 0f), 
                                  new Vector3(1f, -0.016f, 0f), new Vector3(1f, -0.016f, 0f), new Vector3(-0.016f, -0.016f, -1f), 
                                  new Vector3(-0.016f, -0.016f, -1f), new Vector3(-0.016f, -0.016f, -1f), new Vector3(-0.016f, -0.016f, -1f), 
                                  new Vector3(-0.016f, -0.016f, -1f), new Vector3(-0.016f, -0.016f, -1f), new Vector3(-0.016f, -0.016f, -1f), 
                                  new Vector3(-0.016f, -0.016f, 1f), new Vector3(-0.016f, -0.016f, 1f), new Vector3(-0.016f, -0.016f, 1f), 
                                  new Vector3(-0.016f, -0.016f, 1f), new Vector3(-0.016f, -0.016f, 1f), new Vector3(-0.016f, -0.016f, 1f), 
                                  new Vector3(-0.016f, -0.016f, 1f), new Vector3(-0.99f, 0.141f, 0f), new Vector3(-0.99f, 0.141f, 0f), 
                                  new Vector3(-0.955f, 0.297f, 0f), new Vector3(-0.955f, 0.297f, 0f), new Vector3(-0.809f, 0.587f, 0f), 
                                  new Vector3(-0.809f, 0.587f, 0f), new Vector3(-0.333f, 0.937f, -0.109f), 
                                  new Vector3(-0.333f, 0.937f, -0.109f), new Vector3(-0.619f, 0.778f, -0.109f), 
                                  new Vector3(-0.619f, 0.778f, -0.109f), new Vector3(-0.809f, 0.587f, 0f), new Vector3(-0.809f, 0.587f, 0f), 
                                  new Vector3(-0.175f, 0.968f, -0.179f), new Vector3(-0.175f, 0.968f, -0.179f)
                              };

            var uv = new[]
                         {
                             new Vector2(0.9955828f, 0.9955828f), new Vector2(0.9955828f, 0.0004995465f), 
                             new Vector2(0.0004995465f, 0.0004995465f), new Vector2(0.0004995465f, 0.9955828f), 
                             new Vector2(0.0004995465f, 0.0004995465f), new Vector2(0.0004995465f, 0.9955828f), 
                             new Vector2(0.9955828f, 0.9955828f), new Vector2(0.9955828f, 0.0004995465f), new Vector2(0.5842295f, 0.1885468f), 
                             new Vector2(0.8075356f, 0.4118529f), new Vector2(0.9955828f, 0.0004995465f), new Vector2(0.948571f, 0.6900061f), 
                             new Vector2(0.9955828f, 0.9955828f), new Vector2(0.3060763f, 0.04751136f), 
                             new Vector2(0.0004995465f, 0.0004995465f), new Vector2(0.0004995465f, 0.0004995465f), 
                             new Vector2(0.1885468f, 0.4118529f), new Vector2(0.4118529f, 0.1885468f), new Vector2(0.04751136f, 0.6900061f), 
                             new Vector2(0.0004995465f, 0.9955828f), new Vector2(0.6900061f, 0.04751136f), 
                             new Vector2(0.9955828f, 0.0004995465f), new Vector2(0.9955828f, 0.9955828f), 
                             new Vector2(0.9955828f, 0.0004995465f), new Vector2(0.6900061f, 0.0004995465f), 
                             new Vector2(0.6900061f, 0.9955828f), new Vector2(0.4118529f, 0.0004995465f), new Vector2(0.4118529f, 0.9955828f), 
                             new Vector2(0.3060763f, 0.0004995465f), new Vector2(0.3060763f, 0.9955828f), new Vector2(0.5842295f, 0.9955828f), 
                             new Vector2(0.5842295f, 0.0004995465f), new Vector2(0.8075356f, 0.9955828f), 
                             new Vector2(0.8075356f, 0.0004995465f), new Vector2(0.0004995465f, 0.0004995465f), 
                             new Vector2(0.0004995465f, 0.9955828f)
                         };

            var triangles = new[]
                                {
                                    0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4, 8, 9, 10, 9, 11, 10, 11, 12, 10, 10, 13, 8, 10, 14, 13, 15, 16, 17, 15, 
                                    18, 16, 15, 19, 18, 17, 20, 15, 20, 21, 15, 22, 23, 24, 24, 25, 22, 25, 24, 26, 26, 27, 25, 28, 29, 30, 30, 
                                    31, 28, 31, 30, 32, 32, 33, 31, 29, 28, 34, 34, 35, 29 
                                };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "InsideCornerFromCode";
            return mesh;
        }

        #endregion
    }
}