// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Generate Ramp.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides methods that are used to generate various basic mesh shapes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.MeshGeneration
{
    using UnityEngine;

    /// <summary>
    /// Provides methods that are used to generate various basic mesh shapes.
    /// </summary>
    public partial class Generate
    {
        #region Public Methods and Operators

        /// <summary>
        /// Generates a ramp Mesh.
        /// </summary>
        /// <param name="size">
        /// The size of the ramp that will be generated.
        /// </param>
        /// <returns>
        /// A reference to a Mesh object.
        /// </returns>
        public static Mesh Ramp(Vector3 size)
        {
            var mesh = new Mesh();
            var halfX = size.x == 0 ? 0 : size.x / 2f;
            var halfY = size.y == 0 ? 0 : size.y / 2f;
            var halfZ = size.z == 0 ? 0 : size.z / 2f;

            var vertices = new[]
                               {
                                   new Vector3(-halfX, halfY, -halfZ), new Vector3(halfX, -halfY, -halfZ), new Vector3(-halfX, -halfY, -halfZ), 
                                   new Vector3(halfX, -halfY, halfZ), new Vector3(-halfX, halfY, halfZ), new Vector3(-halfX, -halfY, halfZ), 
                                   new Vector3(halfX, -halfY, -halfZ), new Vector3(-halfX, -halfY, halfZ), new Vector3(-halfX, -halfY, -halfZ), 
                                   new Vector3(halfX, -halfY, halfZ), new Vector3(-halfX, -halfY, -halfZ), new Vector3(-halfX, halfY, halfZ), 
                                   new Vector3(-halfX, halfY, -halfZ), new Vector3(-halfX, -halfY, halfZ), new Vector3(-halfX, halfY, -halfZ), 
                                   new Vector3(halfX, -halfY, halfZ), new Vector3(halfX, -halfY, -halfZ), new Vector3(-halfX, halfY, halfZ), 
                               };

            var normals = new[]
                              {
                                  new Vector3(0f, 0f, -1f), new Vector3(0f, 0f, -1f), new Vector3(0f, 0f, -1f), new Vector3(0f, 0f, 1f), 
                                  new Vector3(0f, 0f, 1f), new Vector3(0f, 0f, 1f), new Vector3(0f, -1f, 0f), new Vector3(0f, -1f, 0f), 
                                  new Vector3(0f, -1f, 0f), new Vector3(0f, -1f, 0f), new Vector3(-1f, 0f, 0f), new Vector3(-1f, 0f, 0f), 
                                  new Vector3(-1f, 0f, 0f), new Vector3(-1f, 0f, 0f), new Vector3(1f, 1f, 0f), new Vector3(1f, 1f, 0f), 
                                  new Vector3(1f, 1f, 0f), new Vector3(1f, 1f, 0f), 
                              };

            var uv = new[]
                         {
                             new Vector2(0.0004994869f, 0.9916806f), new Vector2(0.9916806f, 0.0004994869f), 
                             new Vector2(0.0004994869f, 0.0004994869f), new Vector2(0.0004994869f, 0.0004994869f), 
                             new Vector2(0.9916806f, 0.9916806f), new Vector2(0.9916806f, 0.0004994869f), 
                             new Vector2(0.0004994869f, 0.0004994869f), new Vector2(0.9916806f, 0.9916806f), 
                             new Vector2(0.9916806f, 0.0004994869f), new Vector2(0.0004994869f, 0.9916806f), 
                             new Vector2(0.0004994869f, 0.0004994869f), new Vector2(0.9916806f, 0.9916806f), 
                             new Vector2(0.9916806f, 0.0004994869f), new Vector2(0.0004994869f, 0.9916806f), 
                             new Vector2(0.0004994869f, 0.0004994869f), new Vector2(0.9916806f, 0.9916806f), 
                             new Vector2(0.9916806f, 0.0004994869f), new Vector2(0.0004994869f, 0.9916806f), 
                         };

            var triangles = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 9, 10, 11, 12, 11, 10, 13, 14, 15, 16, 17, 15, 14, };

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.uv = uv;
            mesh.triangles = triangles;
            mesh.name = "Codefarts.MeshGeneration.Ramp";
            return mesh;
        }

        #endregion
    }
}