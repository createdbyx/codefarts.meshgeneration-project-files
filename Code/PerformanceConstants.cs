﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if PERFORMANCE
namespace Codefarts.MeshGeneration
{
    /// <summary>
    /// Provides various keys as global constant values for use with the performance testing system.
    /// </summary>
    public class PerformanceConstants
    {                                      
        public const string CreateThreeMaterialBlock = "Generate/Create Multi Material Block";    

        public static string[] GetPerformanceKeys()
        {
            return new[]
                {
                    CreateThreeMaterialBlock,                                     
                };
        }
    }
}
#endif